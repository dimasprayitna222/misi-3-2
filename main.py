from typing import Optional
from fastapi import FastAPI
import json
aplikasi = FastAPI()

#tulis json
artikel = {"judul": ["pisang yang tertukar", "kasurku basah"], "penulis" : "marsubeno"}
with open('artikel.json','w') as json_file: json.dump(artikel, json_file)

#baca json
with open('artikel.json') as f: baca_artikel = json.load(f)
print(baca_artikel)

@aplikasi.get("/")
def status_api():
    return{
        "status" : "Okay"
    }


@aplikasi.post("/articles")
def kirim_artikel():
    #Tangkep lampiran POST dari client
    #Pastiin formatnya JSON
    #Tulis JSON nya ke file
    return{
        "event" : "Kirim Artikel",
        "status" : "berhasil"
    }

@aplikasi.get("/articles")
def lihat_semua_artikel():
    return[{
        "artikel_id" : 3,
        "judul" : "pertapaan keong"
    }, {
        "artikel_id" : 4,
        "judul" : "persembunyian keong"
    }]

@aplikasi.get("/articles/{art_id}")
def lihat_artikel(art_id: int, q: Optional[str] = None):
    judul = "roda zaman menggilas kita, terseret tertatih"
    return{
        "artikel_id" : art_id,
        "judul" : judul,
        "q" : q,
        "event" : "Lihat Artikel",
        "status" : "berhasil",
    }

@aplikasi.delete("/articles/{art_id}")
def hapus_artikel(art_id: int, q: Optional[str] = None):
    return{
        "dokumen_id": art_id,
        "q" : q,
        "event" : "Hapus Artikel",
        "status" : "berhasil"
    }

@aplikasi.put("/articles/{art_id}")
def update_artikel(art_id:int, q: Optional[str] = None):
    #Tangkep lampiran PUT  dari client
    #Pastiin formatnya JSON
    #Buka file JSON
    #Parse file JSON
    #Telusuri list / arraynya yg id artikelnya sama
    #Ganti
    #Tulis kembali ke file JSON
    
    return{
        "event" : "Update Artikel",
        "status" : "berhasil"
    }
